/*
  =======================================================================================
  File: 
        dt_btn_util.h

  Description: 
        Phisical button utility functions to simplify 
        callback registration for button pressed event
        and button released event.

  Author:
       Delphi Tang (唐佐林)
       http://www.dt4sw.com/

  Revision History:
       V0.0.1 (Initial Version)
       V0.0.2 ( 
                1. Do refactory for the interface to encapsulate the vendor SDK details
                2. Move event handler call to a special thread to avoid heavy process in ISR
                3. Trigger long-pressed-event after pressed-event for about 2.5 seconds 
              )
  =======================================================================================

*/

#ifndef DT_BTNUTIL_H
#define DT_BTNUTIL_H

/*
  Description: 
      Button event ID.
*/
typedef enum 
{
    None = 0,
    Pressed = 1,
    LongPressed = 2,
    Released = 4
} BtnEvent;

/*
  Description: 
      Button event callback function pointer type.

  Parameter:
      sender -- string name for the GPIO button
      event  -- event ID which trigger the function call

  Return Value:
      0     -- Success
      other -- Failure
*/
typedef void (*PBtnCallback)(const char* sender, BtnEvent event);

/*
  Description: 
      To initialize button event process context.

  Parameter:
      None

  Return Value:
      0     -- Success
      other -- Failure
*/
int DTButton_Init(void);

/*
  Description: 
      To close button event process context.

  Parameter:
      None

  Return Value:
      None
*/
void DTButton_Deinit(void);

/*
  Description: 
      To register callback functions for a GPIO button.

  Parameter:
      name     -- target GPIO port name for a phisical button
      callback -- callback function for button event
      event    -- the target button event to trigger callback

  Return Value:
      0     -- Success
      other -- Failure
*/
int DTButton_Enable(const char* name, PBtnCallback callback, unsigned int event);

/*
  Description: 
      To unregister callback functions for a GPIO button.

  Parameter:
      name -- target GPIO port name for a phisical button

  Return Value:
      None
*/
void DTButton_Disable(const char* name);

#endif